﻿#include <iostream>
#include <string>

class Player
{
private:
    int ScorePlayer;
    std::string NamePlayer;
public:
    void WriteData(std::string _Name, int _Score)
    {
        NamePlayer = _Name;
        ScorePlayer = _Score;
    }
    
    int GetScore()
    {
        return ScorePlayer;
    }

    std::string GetName()
    {
        return NamePlayer;
    }

};


int main()
{
    int Limit, Score, TempScore;
    std::string Name, TempName;

    std::cout << "Enter the number of Plaers: ";
    std::cin >> Limit;

    Player *Players = new Player[Limit];
    for (int i = 0; i < Limit; i++)
    {
        std::cout << "Enter Name:  " << i+1 << " player: ";
        std::cin >> TempName;
        std::cout << "Enter score: " << i+1 << " player: ";
        std::cin >> TempScore;
        Players[i].WriteData(TempName, TempScore);
    }
    int TempLimit = Limit;
    while (TempLimit != 0)
    {
        for (int i = 0; i < TempLimit; i++)
        {
            if (Players[i + 1].GetScore() > Players[i].GetScore())
            {
                TempName = Players[i + 1].GetName();
                TempScore = Players[i + 1].GetScore();
                Players[i + 1].WriteData(Players[i].GetName(), Players[i].GetScore());
                Players[i].WriteData(TempName, TempScore);
               
            }
        }
        TempLimit -= 1;
    }

    
    for (int i = 0; i < Limit; i++)
    {
        std::cout << Players[i].GetName() << ' ';
        std::cout << Players[i].GetScore() << std::endl;
    }

    delete[] Players;
}
